package com.example.restapi.latihan7restapi.entity;

import com.example.restapi.latihan7restapi.entity.Abstract.AbstractDate;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "employee_training")
public class EmployeeTraining extends AbstractDate implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(targetEntity = Employee.class, cascade = CascadeType.ALL)
    private Employee employee;

    @ManyToOne(targetEntity = Training.class, cascade = CascadeType.ALL)
    private Training training;


}
